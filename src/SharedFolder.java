import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class SharedFolder {
	private static File sharedFolder;

	public SharedFolder() {
		sharedFolder = new File("C://shared/Network Workspace/Folder");												//initializes shared folder which is static because all classes should use same folder
	}
	public String toString(){																						//to string method
		return sharedFolder.toString();
	}
	
	
	private boolean validFile(String param) {
		if (param.endsWith(".txt") || param.endsWith(".png") || param.endsWith(".jpeg") || param.endsWith(".html")	//checks if file is valid aka it can be displayed by browser
				|| param.endsWith(".htm") || param.endsWith(".jpg") || param.endsWith(".png")) {
			return true;
		} else {
			return false;
		}

	}

	public byte[] getFile(String route) throws IOException, RestrictedAccessException, InvalidFileException, NullPointerException {
		File temp = new File(sharedFolder.toString() + route);														//returns file as byte array
		if (temp.exists()) {																						//if file actually exists
			RandomAccessFile read;
			byte[] arr;																								

			if (temp.isFile()) {																					//if file not a directory
				if(validFile(route)){																				//checks if file is valid
				read = new RandomAccessFile(temp, "r");																//reads file and returns it as byte array
				arr = new byte[(int) temp.length()];
				read.read(arr);
				read.close();
				return arr;
				}else{																								//if it is not a valid file, throws invalid file exception
					throw new InvalidFileException();
				}
			} else {
				String[] fileArray = temp.list();																	//if it is a directory, checks for index.html or index.htm files
				for (String string : fileArray) {
					if (string.toLowerCase().equals("index.html") || string.toLowerCase().equals("index.htm")) {	//if they exist then returns content
						File index = new File(sharedFolder.toString() + route +"\\"+ string);
						System.out.println("true");
						read = new RandomAccessFile(index, "r");
						arr = new byte[(int) index.length()];
						read.read(arr);
						read.close();
						return arr;
					}
				}
				throw new RestrictedAccessException();																//else throws new RestrictedAccessException since the user should not be able to browse content 
			}
		}else{
			throw new FileNotFoundException();																		//and if file was not found, throws 404 
		}
	}
	public void saveImage(BufferedImage img, String name) throws IOException{										//saves image 
		File file = new File(this.toString()+"//images//"+name+".png");
		ImageIO.write(img,"png",file);
	}
	public boolean fileExists(String fileName){
		File f = new File(sharedFolder.toString() + "//images//" + fileName+".png");								//checks if file exists
		if(f.exists()){
			return true;
		}else {
			return false;
		}
	}
}

// throw new RestrictedAccessException(); //if directory doesnt have
// index.hmtl it should not be seen.
// Displays all files and folders inside directory
// for(int i = 0; i< fileArray.length; i++){
// fileArray[i] = "<body><p><a href = '"+route +"/"+fileArray[i] +"'>" +
// fileArray[i]+"</a></p></body>";
// }
// return toString(fileArray).getBytes();