import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class SocketThread implements Runnable {
	private Socket socket;
	int id;
	private static int ID = 0;

	private ArrayList<byte[]> findParts(byte[] original) throws UnsupportedEncodingException {
		byte[] firstTemp;
		byte[] secondTemp;
		ArrayList<byte[]> list = new ArrayList<byte[]>();
		for (int i = 0; i < original.length; i++) {
			if (original[i] == 13 && original[i + 1] == 10 && original[i + 2] == 13 && original[i + 3] == 10) { // goes
																												// through
																												// all
																												// elements
																												// and
																												// looks
																												// for
																												// 2
																												// consecutive
																												// new
																												// lines
				firstTemp = new byte[i]; // which are represented by 13 10 13 10
				secondTemp = new byte[original.length - i - 4]; // if found
																// copies
																// elements to
																// another byte
																// array
				firstTemp = Arrays.copyOfRange(original, 0, i);													
				secondTemp = Arrays.copyOfRange(original, i + 4, original.length);								//and ands other bytes to second arrray
				list.add(firstTemp);																			//adds both byte arrays to arraylist
				list.add(secondTemp);																			//and breaks list
				break;
			}
		}
		return list;
	}

	public SocketThread(Socket socket) {
		this.socket = socket;
		id = ID++;
	}

	private void newLine(DataOutputStream out) {													//sends new line into output stream
		String newLine = "\r\n";
		try {
			out.write(newLine.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String[] getParams(String parameter) {													//gets parameters		
		String[] arr = parameter.split("\\n")[0].split(" ");
		return Arrays.copyOfRange(arr, 0, 2);
	}

	// out.write("<html><body><p>new paragraph</p></body></html>".getBytes());

	

	private String getName(String full) {															//gets name of file when request is passed
		String[] temp = full.split("filename=\"");
		return temp[1].toLowerCase().split(".png")[0];
	}

	@Override
	public void run() {
		// Response list
		String response = "HTTP/1.0 200 OK\r\n";											
		String notFound = "HTTP/1.0 404 Not Found\r\n";
		String restrictedAccess = "HTTP/1.0 403 Forbiden access\r\n";
		String internalError = "HTTP/1.0 500 Internal Server Error\r\n";
		String unprocessableEntity = "HTTP/1.0 422  Unprocessable Entity\r\n";
		String notImplemented = "HTTP/1.0 501  Unimplemented request\r\n";
		String accepted = "HTTP/1.0 202  accepted \r\n";
		String created = "HTTP/1.0 201  Created \r\n";
		String notModified = "HTTP/1.0 304  Not Modified \r\n";

		try {
	
			SharedFolder shared = new SharedFolder();								//Fields
			
			byte[] buf = new byte[1024];
			byte[] arr;
			String input;
			String[] parameters;

			DataInputStream in = new DataInputStream(socket.getInputStream()); // creates
																				// new
																				// input
																				// and
																				// output
																				// streams
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			in.read(buf);

			input = new String(buf, "UTF-8").trim();							//gets request
			System.out.println(input);
			
			parameters = getParams(input);										//checks parameter
			// get request
			if (parameters[0].toUpperCase().equals("GET")) {									//if parameter is get
				try {																				
					// byte[] temp = new byte[50000];
					try {
						arr = shared.getFile(parameters[1]);									//tries to get file for shared folder in bytes and send it as response
						out.write(response.getBytes());
						newLine(out);
						out.write(arr);
						out.write("\r\n".getBytes());
					} catch (InvalidFileException e) {											//if sharedFolder throws invalid fileException, sends unprocessed entity response
						out.write(unprocessableEntity.getBytes());
						newLine(out);
						out.write("<h1>File cannot be displayed</h1> \r\n".getBytes());
					}

				} catch (FileNotFoundException e) {												//if file doesnt exist, sends 404
					System.out.println("File not Found");
					out.write(notFound.getBytes());
					newLine(out);
					out.write("<h1>404 file not found</h1>".getBytes());
				} catch (RestrictedAccessException e) {											//if file exists, but it is a folder without index.html, shows
					System.out.println("Restricted  acceess");									//restricted access status
					out.write(restrictedAccess.getBytes());
					newLine(out);

					out.write("You do not have permission to view this file".getBytes());
				}
			} else if (parameters[0].toUpperCase().equals("POST")) {							// if request is post
				//fields
				String fileName;	
				BufferedImage img;
				ArrayList<byte[]> byteList;
				System.out.println("=====================POST===========================");
				byte[] read = new byte[200000];									
				in.read(read);																	//reads input
				byteList = findParts(read);														//divides message into 2 parts, the message and image, which are separated by 2 new lines
				fileName = getName(new String(byteList.get(0), "UTF-8"));						//gets file name
				img = ImageIO.read(new ByteArrayInputStream(byteList.get(1)));					//creates image
				shared.saveImage(img, fileName);												//saves image with given name
				out.write(accepted.getBytes());													//sends response
				newLine(out);
				out.write("File has been uploaded successfully".getBytes());					//sends response


			} else if (parameters[0].toUpperCase().equals("PUT")) {								//put request
				//fields
				String fileName;
				BufferedImage img;
				ArrayList<byte[]> byteList;
				System.out.println("=====================POST===========================");
				byte[] read = new byte[200000];
				in.read(read);																	//reads messsage
				byteList = findParts(read);	
				fileName = getName(new String(byteList.get(0), "UTF-8"));						//checks if file exists
				if (!shared.fileExists(fileName)) {												//if it doesnt exist, then creates image and posts
					img = ImageIO.read(new ByteArrayInputStream(byteList.get(1)));
					shared.saveImage(img, fileName);
					out.write(created.getBytes());
					newLine(out);
					out.write("File has been uploaded successfully".getBytes());				//sends response
				} else {																		// if it exists sends not modified status
					out.write(notModified.getBytes());
					newLine(out);
					out.write("File already exists".getBytes());								//sends response
				}
				System.out.println("=====================PUT===========================");
			} else {
				out.write(notImplemented.getBytes());											//if request is not valid
				newLine(out);																	//sends not implemented response
				out.write("<h1>The request is not valid or not implemented yet</h1>".getBytes());
			}

		} catch (IOException | NullPointerException e) {
			try {
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());			//tries to send error message
				out.write(internalError.getBytes());											
				newLine(out);
				out.write("Internal server error".getBytes());											

			} catch (IOException e1) {
			}

		}
		try {
			socket.close();																		//closes socket
		} catch (IOException e) {
			System.out.println("Unable to close socket");
		}
	}


}
