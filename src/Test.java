import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class Test {

	public static void main(String[] arr) {
		try {

			byte[] imageInByte;
			BufferedImage originalImage = ImageIO.read(new File("C://shared/network workspace/Folder/Untitled.png"));

			// convert BufferedImage to byte array
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(originalImage, "jpg", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			System.out.println(Arrays.toString(imageInByte));
			baos.close();


		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
