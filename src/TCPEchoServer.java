
// File Name GreetingServer.java

import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.io.*;

public class TCPEchoServer{
	private ServerSocket serverSocket;											//variables
	private ExecutorService executor;
	private Lock lock;
	
	//constructor
	public TCPEchoServer(int port) throws IOException {							//constructor
		executor = Executors.newCachedThreadPool();
		serverSocket = new ServerSocket(4950);
		lock = new ReentrantLock();
	}
	//run method
	public void run() {
		System.out.println("Server is running");								
		while (true) {															//runs server nonstop
			try {
				
																				//need to lock in order to prevent loses of requests
				lock.lock();													
				executor.execute(new SocketThread(serverSocket.accept()));
				lock.unlock();
			} catch (SocketTimeoutException s) {
				System.out.println("Socket timed out!");
				break;
			} catch (IOException e) {
				System.out.println("test");
				break;
			}
		}
	}

	public static void main(String[] args) {
		int port = 4950;
		 
			TCPEchoServer server;
			try {
				server = new TCPEchoServer(port);
				server.run();

			} catch (IOException e) {
				System.out.println("Server as unable to start");
			}
	
	}
}
